from django.db import models

class Song(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to="songs")
    listened_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title