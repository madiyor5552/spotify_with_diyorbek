from django.shortcuts import render
from rest_framework.views import APIView
from music.models import Song
from rest_framework.response import Response
from music.serializers import SongSerializer

class SongsAPIView(APIView):
    
    def get(self,request):
        songs = Song.objects.all()
        tarjima_qilngan_qoshiqlar = SongSerializer(songs,many=True)
        
        return Response(tarjima_qilngan_qoshiqlar.data)
